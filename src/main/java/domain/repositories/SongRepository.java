package domain.repositories;

import com.jasongoodwin.monads.Try;
import data.gateways.SongGateway;
import domain.objects.Song;
import libs.exceptions.DuplicateException;
import libs.providers.object.Objects;

import java.util.List;

public class SongRepository {
    private SongGateway gateway = Objects.create(SongGateway.class);

    public Try<List<Song>> getInitial() {
        return gateway.getInitial(Song.class);
    }

    public Try<Song> insert(Song song) {
        return Try.ofFailable(() -> song)
                .filter(s -> !gateway.alreadyExists(s.Name).getUnchecked())
                .recoverWith(e -> Try.failure(DuplicateException.of()))
                .flatMap(s -> gateway.insert(song));
    }

    //TODO exists because initial list contains duplicates
    public Try<Song> insertInitial(Song song) {
        return Try.ofFailable(() -> song)
                .flatMap(s -> gateway.insert(song));
    }

    public Try<Song> update(Song song) {
        return Try.ofFailable(() -> song)
                .flatMap(s -> gateway.update(s, s.Id));
    }

    public Try<Song> delete(Song artist) {
        return Try.ofFailable(() -> artist)
                .flatMap(s -> gateway.delete(s, s.Id));
    }

    public Try<List<Song>> findByGenre(String genre) {
        return gateway.findByGenre(Song.class, genre);
    }
}
