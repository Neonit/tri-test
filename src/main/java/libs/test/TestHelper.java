package libs.test;

import libs.providers.object.MockProvider;
import libs.providers.object.Objects;
import org.junit.jupiter.api.BeforeEach;


public class TestHelper {

    @BeforeEach
    private void setupProviders() {
        Objects.provide(new MockProvider());
        before();
    }

    @BeforeEach
    public void before() {

    }
}
