package data.gateways;


import com.jasongoodwin.monads.Try;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import data.setup.MongoDatabase;
import libs.Json;
import org.apache.commons.io.IOUtils;
import org.bson.Document;

import java.util.List;

public class ArtistGateway {

    private final MongoCollection<Document> collection = MongoDatabase.getCollection("artists");

    public <T> Try<T> findByName(Class<T> type, String name) {
        return Try.ofFailable(() -> name)
                .map(n -> collection.find(Filters.eq("Name", n)).first())
                .flatMap(d -> Json.toObject(d.toJson(), type));

    }

    public <T> Try<T> insert(T object) {
        return Try.ofFailable(() -> object)
                .flatMap(o -> Json.toJson(o))
                .map(j -> {
                    collection.insertOne(Document.parse(j));
                    return object;
                });
    }

    public Try<Boolean> alreadyExists(String name) {
        return Try.ofFailable(() -> name)
                .map(n -> collection.countDocuments(new Document("Name", n)))
                .map(i -> i != 0);
    }

    public <T> Try<List<T>> getInitial(Class<T> type) {
        return getContentFromInitialFile()
                .flatMap(s -> convertToList(type, s));
    }

    public <T> Try<T> update(T object, String id) {
        return Try.ofFailable(() -> object)
                .flatMap(o -> Json.toJson(o))
                .map(j -> collection.replaceOne(Filters.eq("Id", id), Document.parse(j)))
                .map(r -> object);
    }

    public <T> Try<T> delete(T object, String id) {
        return Try.ofFailable(() -> object)
                .flatMap(o -> Json.toJson(o))
                .map(j -> collection.deleteOne(Filters.eq("Id", id)))
                .map(r -> object);
    }

    private Try<String> getContentFromInitialFile() {
        return Try.ofFailable(() -> getClass().getClassLoader().getResource("artists.json"))
                .map(r -> IOUtils.toByteArray(r))
                .map(b -> new String(b));
    }

    private <T> Try<List<T>> convertToList(Class<T> type, String json) {
        return Try.ofFailable(() -> json)
                .flatMap(j -> Json.toList(j, type));
    }
}
