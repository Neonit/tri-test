package domain.repositories;

import com.jasongoodwin.monads.Try;
import data.gateways.ArtistGateway;
import domain.objects.Artist;
import domain.objects.ArtistTest;
import libs.providers.object.Objects;
import libs.test.TestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;


class ArtistRepositoryTest extends TestHelper {
    private ArtistGateway gateway;
    private Artist artist;
    private List<Artist> artists;

    @BeforeEach
    public void before() {
        gateway = Objects.create(ArtistGateway.class);

        artist = ArtistTest.dummy();
        artists = Arrays.asList(artist);
    }

    @Test
    public void insertSucceeds() {
        when(gateway.insert(artist)).thenReturn(Try.ofFailable(() -> artist));
        when(gateway.alreadyExists(artist.Name)).thenReturn(Try.ofFailable(() -> false));
        Try<Artist> result = new ArtistRepository().insert(artist);
        assertTrue(result.isSuccess());
    }

    @Test
    public void insertDuplicateFails() {
        when(gateway.alreadyExists(artist.Name)).thenReturn(Try.ofFailable(() -> true));
        Try<Artist> result = new ArtistRepository().insert(artist);
        assertFalse(result.isSuccess());
    }

    @Test
    public void getInitialSucceeds() {
        when(gateway.getInitial(Artist.class)).thenReturn(Try.ofFailable(() -> artists));
        Try<List<Artist>> result = new ArtistRepository().getInitial();
        assertTrue(result.isSuccess());
        assertThat(result.getUnchecked(), is(artists));
    }

    @Test
    public void findByNameSucceeds() {
        when(gateway.findByName(Artist.class, artist.Name)).thenReturn(Try.ofFailable(() -> artist));
        Try<Artist> result = new ArtistRepository().findByName(artist.Name);
        assertTrue(result.isSuccess());
        assertThat(result.getUnchecked(), is(artist));
    }

    @Test
    public void updateSucceeds() {
        when(gateway.update(artist, artist.Id)).thenReturn(Try.ofFailable(() -> artist));
        Try<Artist> result = new ArtistRepository().update(artist);
        assertTrue(result.isSuccess());
        assertThat(result.getUnchecked(), is(artist));
    }

    @Test
    public void deleteSucceeds() {
        when(gateway.delete(artist, artist.Id)).thenReturn(Try.ofFailable(() -> artist));
        Try<Artist> result = new ArtistRepository().delete(artist);
        assertTrue(result.isSuccess());
        assertThat(result.getUnchecked(), is(artist));
    }
}