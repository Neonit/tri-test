package services.resources;

import com.jasongoodwin.monads.Try;
import domain.objects.Artist;
import io.undertow.server.HttpServerExchange;
import libs.Json;
import libs.providers.object.Objects;
import org.apache.commons.io.IOUtils;
import org.xnio.streams.ChannelInputStream;
import process.usecases.CreateArtist;
import process.usecases.DeleteArtist;
import process.usecases.FindArtist;
import process.usecases.UpdateArtist;

public class ArtistsResource {

    private final FindArtist findArtist = Objects.create(FindArtist.class);
    private final CreateArtist createArtist = Objects.create(CreateArtist.class);
    private final UpdateArtist updateArtist = Objects.create(UpdateArtist.class);
    private final DeleteArtist deleteArtist = Objects.create(DeleteArtist.class);

    public Try<String> createResponse(HttpServerExchange exchange) {
        //TODO add Artist/{id} resource
        switch(exchange.getRequestMethod().toString()) {
            case "GET": return findByName(exchange);
            case "POST": return createArtist(exchange);
            case "PUT": return updateArtist(exchange);
            case "DELETE": return deleteArtist(exchange);
            default: return Try.ofFailable(() -> "Oops, something went wrong");
        }
    }

    //http://localhost:8080/artists?name=Metallica
    public Try<String> findByName(HttpServerExchange exchange) {
        return Try.ofFailable(() -> exchange.getQueryParameters().get("name").getFirst())
                .flatMap(n -> findArtist.findArtist(n))
                .flatMap(j -> Json.toJson(j));
    }

    //http://localhost:8080/artists
    public Try<String> createArtist(HttpServerExchange exchange) {
        return Try.ofFailable(() -> new ChannelInputStream(exchange.getRequestChannel()))
                .map(is -> IOUtils.toString(is))
                .flatMap(j -> Json.toObject(j, Artist.class))
                .flatMap(a -> createArtist.createArtist(a))
                .flatMap(a -> Json.toJson(a));
    }

    //http://localhost:8080/artists
    public Try<String> updateArtist(HttpServerExchange exchange) {
        return Try.ofFailable(() -> new ChannelInputStream(exchange.getRequestChannel()))
                .map(is -> IOUtils.toString(is))
                .flatMap(j -> Json.toObject(j, Artist.class))
                .flatMap(a -> updateArtist.updateArtist(a))
                .flatMap(a -> Json.toJson(a));
    }

    //http://localhost:8080/artists
    public Try<String> deleteArtist(HttpServerExchange exchange) {
        return Try.ofFailable(() -> new ChannelInputStream(exchange.getRequestChannel()))
                .map(is -> IOUtils.toString(is))
                .flatMap(j -> Json.toObject(j, Artist.class))
                .flatMap(a -> deleteArtist.deleteArtist(a))
                .flatMap(a -> Json.toJson(a));
    }
}
