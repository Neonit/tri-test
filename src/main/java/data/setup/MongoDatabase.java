package data.setup;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.bson.Document;

import java.io.IOException;

public class MongoDatabase {

    private static MongoClient mongoClient;
    private MongodExecutable mongodExecutable;
    public static final String IP = "localhost";
    public static final int PORT = 12345;

    public static synchronized MongoClient getClient() {
        return mongoClient = mongoClient != null ? mongoClient : new MongoClient(IP, PORT);
    }

    public static synchronized MongoCollection<Document> getCollection(String collection) {
        return MongoDatabase.getClient().getDatabase("test").getCollection(collection);
    }

    public void startMongo() throws IOException {
        MongodStarter starter = MongodStarter.getDefaultInstance();

        IMongodConfig mongodConfig = new MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(new Net(IP, PORT, Network.localhostIsIPv6()))
                .build();

        mongodExecutable = null;
        mongodExecutable = starter.prepare(mongodConfig);
        MongodProcess mongod = mongodExecutable.start();
    }

    public void stopMongo() {
        if (mongodExecutable != null)
            mongodExecutable.stop();
    }
}
