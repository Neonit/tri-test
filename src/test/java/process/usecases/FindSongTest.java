package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Song;
import domain.objects.SongTest;
import domain.repositories.SongRepository;
import libs.providers.object.Objects;
import libs.test.TestHelper;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class FindSongTest extends TestHelper {
    private Song song;
    private List<Song> songs;
    private SongRepository songRepository;

    @Override
    public void before() {
        songRepository = Objects.create(SongRepository.class);
        song = SongTest.dummy();
        songs = Arrays.asList(song);
    }

    @Test
    public void findSucceeds() {
        when(songRepository.findByGenre(song.Genre)).thenReturn(Try.successful(songs));
        Try<List<Song>> result = new FindSong().findSong(song.Genre);
        assertTrue(result.isSuccess());
        assertTrue(result.getUnchecked().get(0).equals(song));
        verify(songRepository, new Times(1)).findByGenre(song.Genre);
    }
}