package domain.objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Song {

    //TODO resolve _id issue
    @JsonIgnore
    public String _id;
    public final String Id;
    public final String Name;
    public final String Year;
    public final String Artist;
    public final String Shortname;
    public final String Bpm;
    public final String Duration;
    public final String Genre;
    public final String SpotifyId;
    public final String Album;

    private Song() {
        this(null, null, null, null, null, null, null, null, null, null);
    }

    private Song(String id,
                 String name,
                 String year,
                 String artist,
                 String shortname,
                 String bpm,
                 String duration,
                 String genre,
                 String spotifyId,
                 String album
                 ) {
        this.Id = id;
        //TODO remove once _id issue is resolved
        this._id = id;
        this.Name = name;
        this.Year = year;
        this.Artist = artist;
        this.Shortname = shortname;
        this.Bpm = bpm;
        this.Duration = duration;
        this.Genre = genre;
        this.SpotifyId = spotifyId;
        this.Album = album;
    }

    public static Song of(String id,
                          String name,
                          String year,
                          String artist,
                          String shortname,
                          String bpm,
                          String duration,
                          String genre,
                          String spotifyId,
                          String album
    ) {
        return new Song(id,
                name,
                year,
                artist,
                shortname,
                bpm,
                duration,
                genre,
                spotifyId,
                album
        );
    }

    //TODO add validation
}
