package domain.objects;

import com.jasongoodwin.monads.Try;
import libs.Json;
import libs.test.TestHelper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ArtistTest extends TestHelper {

    private Artist artist = dummy();

    public static Artist dummy() {
        return Artist.of("Id", "Name");
    }

    @Test
    public void jacksonTest() {
        Try<String> input = Json.toJson(artist);
        assertTrue(input.isSuccess());
        Try<Artist> output = Json.toObject(input.getUnchecked(), Artist.class);
        assertTrue(output.isSuccess());
        assertEquals(output.getUnchecked().Id, artist.Id);
        assertEquals(output.getUnchecked().Name, artist.Name);
    }
}