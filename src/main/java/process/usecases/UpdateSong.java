package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Song;
import domain.repositories.SongRepository;
import libs.providers.object.Objects;

public class UpdateSong {
    private SongRepository songRepository = Objects.create(SongRepository.class);

    public Try<Song> updateSong(Song song) {
        return songRepository.update(song);
    }
}
