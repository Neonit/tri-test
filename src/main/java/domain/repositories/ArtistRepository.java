package domain.repositories;

import com.jasongoodwin.monads.Try;
import data.gateways.ArtistGateway;
import domain.objects.Artist;
import libs.exceptions.DuplicateException;
import libs.providers.object.Objects;

import java.util.List;

public class ArtistRepository {
    private ArtistGateway gateway = Objects.create(ArtistGateway.class);

    public Try<Artist> findByName(String name) {
        return gateway.findByName(Artist.class, name);
    }

    public Try<List<Artist>> getInitial() {
        return gateway.getInitial(Artist.class);
    }

    public Try<Artist> insert(Artist artist) {
        return Try.ofFailable(() -> artist)
                .filter(a -> !gateway.alreadyExists(a.Name).getUnchecked())
                .recoverWith(e -> Try.failure(DuplicateException.of()))
                .flatMap(a -> gateway.insert(artist));
    }

    public Try<Artist> update(Artist artist) {
        return Try.ofFailable(() -> artist)
                .flatMap(a -> gateway.update(a, a.Id));
    }

    public Try<Artist> delete(Artist artist) {
        return Try.ofFailable(() -> artist)
                .flatMap(a -> gateway.delete(a, a.Id));
    }
}
