package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Artist;
import domain.objects.ArtistTest;
import domain.repositories.ArtistRepository;
import libs.providers.object.Objects;
import libs.test.TestHelper;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class DeleteArtistTest extends TestHelper {

    private ArtistRepository artistRepository;
    private Artist artist;

    @Override
    public void before() {
        artistRepository = Objects.create(ArtistRepository.class);
        artist = ArtistTest.dummy();
    }

    @Test
    public void deleteSucceeds() {
        when(artistRepository.delete(artist)).thenReturn(Try.successful(artist));
        Try<Artist> result = new DeleteArtist().deleteArtist(artist);
        assertTrue(result.isSuccess());
        assertTrue(result.getUnchecked().equals(artist));
        verify(artistRepository, new Times(1)).delete(artist);
    }
}