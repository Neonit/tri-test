package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Artist;
import domain.repositories.ArtistRepository;
import libs.providers.object.Objects;

public class UpdateArtist {
    private ArtistRepository artistRepository = Objects.create(ArtistRepository.class);

    public Try<Artist> updateArtist(Artist artist) {
        return artistRepository.update(artist);
    }
}
