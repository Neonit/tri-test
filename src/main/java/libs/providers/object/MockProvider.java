package libs.providers.object;

import org.mockito.Mockito;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MockProvider implements ObjectProvider {
    private Map<Class, Object> objects = new HashMap<>();
    private List<Class> classesToMock;

    @Override
    public <T> T create(Class<T> type, Object... params) {

        if (classesToMock != null && !classesToMock.contains(type)) {
            return null;
        }

        if (!objects.containsKey(type)) {
            objects.put(type, Mockito.mock(type));
        }

        return (T) objects.get(type);
    }
}
