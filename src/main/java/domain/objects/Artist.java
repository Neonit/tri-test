package domain.objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Artist {

    //TODO resolve _id issue
    @JsonIgnore
    public String _id;
    public final String Id;
    public final String Name;

    private Artist() {
        this(null, null);
    }

    private Artist(String id,
                   String name
    ) {
        this.Id = id;
        //TODO remove once _id issue is resolved
        this._id = id;
        this.Name = name;
    }

    public static Artist of(String id,
                            String name
    ) {
        return new Artist(id,
                name);
    }

    //TODO add validation
}
