package services.application;

import data.setup.MongoDatabase;
import io.undertow.Undertow;
import libs.providers.object.Objects;
import libs.providers.object.SImpleObjectProvider;
import process.usecases.SetupInitial;
import services.resources.RootResource;

import java.io.IOException;

public class TestApplication {

    public static void main(String[] args) throws IOException {
        Objects.provide(new SImpleObjectProvider());
        MongoDatabase mongoDatabase = new MongoDatabase();
        mongoDatabase.startMongo();
        new SetupInitial().setupInitial();
        Undertow server = Undertow.builder()
                .addHttpListener(8080, "localhost")
                .setHandler(new RootResource())
                .build();
        server.start();
    }
}
