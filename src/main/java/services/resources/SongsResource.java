package services.resources;

import com.jasongoodwin.monads.Try;
import domain.objects.Song;
import io.undertow.server.HttpServerExchange;
import libs.Json;
import libs.providers.object.Objects;
import org.apache.commons.io.IOUtils;
import org.xnio.streams.ChannelInputStream;
import process.usecases.CreateSong;
import process.usecases.DeleteSong;
import process.usecases.FindSong;
import process.usecases.UpdateSong;

public class SongsResource {

    private final FindSong findSong = Objects.create(FindSong.class);
    private final CreateSong createSong = Objects.create(CreateSong.class);
    private final UpdateSong updateSong = Objects.create(UpdateSong.class);
    private final DeleteSong deleteSong = Objects.create(DeleteSong.class);

    public Try<String> createResponse(HttpServerExchange exchange) {
        //TODO add Song/{id} resource
        switch(exchange.getRequestMethod().toString()) {
            case "GET": return findByGenre(exchange);
            case "POST": return createSong(exchange);
            case "PUT": return updateSong(exchange);
            case "DELETE": return deleteSong(exchange);
            default: return Try.ofFailable(() -> "Oops, something went wrong");
        }
    }

    //http://localhost:8080/songs?genre=Metal
    public Try<String> findByGenre(HttpServerExchange exchange) {
        return Try.ofFailable(() -> exchange.getQueryParameters().get("genre").getFirst())
                .flatMap(g -> findSong.findSong(g))
                .flatMap(j -> Json.toJson(j));
    }

    //http://localhost:8080/songs
    public Try<String> createSong(HttpServerExchange exchange) {
        return Try.ofFailable(() -> new ChannelInputStream(exchange.getRequestChannel()))
                .map(is -> IOUtils.toString(is))
                .flatMap(j -> Json.toObject(j, Song.class))
                .flatMap(s -> createSong.createSong(s))
                .flatMap(s -> Json.toJson(s));
    }

    //http://localhost:8080/artists
    public Try<String> updateSong(HttpServerExchange exchange) {
        return Try.ofFailable(() -> new ChannelInputStream(exchange.getRequestChannel()))
                .map(is -> IOUtils.toString(is))
                .flatMap(j -> Json.toObject(j, Song.class))
                .flatMap(s -> updateSong.updateSong(s))
                .flatMap(s -> Json.toJson(s));
    }

    //http://localhost:8080/artists
    public Try<String> deleteSong(HttpServerExchange exchange) {
        return Try.ofFailable(() -> new ChannelInputStream(exchange.getRequestChannel()))
                .map(is -> IOUtils.toString(is))
                .flatMap(j -> Json.toObject(j, Song.class))
                .flatMap(s -> deleteSong.deleteSong(s))
                .flatMap(s -> Json.toJson(s));
    }
}
