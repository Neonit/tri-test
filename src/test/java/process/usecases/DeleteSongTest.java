package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Song;
import domain.objects.SongTest;
import domain.repositories.SongRepository;
import libs.providers.object.Objects;
import libs.test.TestHelper;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class DeleteSongTest extends TestHelper {

    private SongRepository songRepository;
    private Song song;

    @Override
    public void before() {
        songRepository = Objects.create(SongRepository.class);
        song = SongTest.dummy();
    }

    @Test
    public void deleteSucceeds() {
        when(songRepository.delete(song)).thenReturn(Try.successful(song));
        Try<Song> result = new DeleteSong().deleteSong(song);
        assertTrue(result.isSuccess());
        assertTrue(result.getUnchecked().equals(song));
        verify(songRepository, new Times(1)).delete(song);
    }

}