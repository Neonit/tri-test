package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Song;
import domain.repositories.SongRepository;
import libs.providers.object.Objects;

import java.util.List;

public class FindSong {
    private SongRepository songRepository = Objects.create(SongRepository.class);

    public Try<List<Song>> findSong(String genre) {
        return songRepository.findByGenre(genre);
    }
}
