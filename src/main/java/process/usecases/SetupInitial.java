package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Song;
import domain.repositories.ArtistRepository;
import domain.repositories.SongRepository;
import libs.providers.object.Objects;

import java.util.List;
import java.util.stream.Collectors;

public class SetupInitial {
    private SongRepository songRepository = Objects.create(SongRepository.class);
    private ArtistRepository artistRepository = Objects.create(ArtistRepository.class);

    private CreateArtist createArtist = Objects.create(CreateArtist.class);

    public Try<Boolean> setupInitial() {
        return setupInitialSongs()
                .flatMap(ss -> getMetalArtistNames(ss))
                .map(ns -> artistRepository.getInitial().get().stream()
                        .filter(a -> ns.contains(a.Name))
                        .collect(Collectors.toList()))
                .map(as -> as.stream()
                        .map(a -> createArtist.createArtist(a).getUnchecked()).collect(Collectors.toList()))
                .map(ss -> true);
    }

    private Try<List<Song>> setupInitialSongs() {
        return songRepository.getInitial()
                .map(ss -> ss.stream()
                        .filter(s -> s.Genre.contains("Metal") && (Integer.valueOf(s.Year) < 2016))
                        .collect(Collectors.toList()))
                .map(ss -> ss.stream()
                        .map(s -> songRepository.insertInitial(s).getUnchecked()).collect(Collectors.toList()));
    }

    private Try<List<String>> getMetalArtistNames(List<Song> songs) {
        return Try.ofFailable(() -> songs)
                .map(ss -> ss.stream()
                        .map(s -> s.Artist)
                        .distinct()
                        .collect(Collectors.toList()));
    }
}
