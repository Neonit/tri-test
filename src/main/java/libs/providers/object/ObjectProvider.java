package libs.providers.object;

public interface ObjectProvider {

    <T> T create(Class<T> type, Object... params);
}