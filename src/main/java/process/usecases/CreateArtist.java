package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Artist;
import domain.repositories.ArtistRepository;
import libs.providers.object.Objects;

public class CreateArtist {
    private ArtistRepository artistRepository = Objects.create(ArtistRepository.class);

    public Try<Artist> createArtist(Artist artist) {
        return artistRepository.insert(artist);
    }
}
