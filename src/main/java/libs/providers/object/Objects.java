package libs.providers.object;

import libs.providers.Providers;

public class Objects {

    private static final Providers<ObjectProvider> providers = new Providers<>();

    public static void provide(ObjectProvider... p) {
        providers.add(p);
    }

    public static <T> T create(Class<T> type, Object... params) {
        return providers.map(p -> p.create(type, params)).getUnchecked();
    }
}
