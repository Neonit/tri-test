package libs.providers.object;


import java.lang.reflect.Array;

public class SImpleObjectProvider implements ObjectProvider {
    @Override
    public <T> T create(Class<T> type, Object... params) {
        try {
            return type.isArray()
                    ? (T) Array.newInstance(type.getComponentType(), 0)
                    : type.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }
}
