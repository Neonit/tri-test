package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Song;
import domain.objects.SongTest;
import domain.repositories.SongRepository;
import libs.providers.object.Objects;
import libs.test.TestHelper;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CreateSongTest extends TestHelper {
    private Song song;
    private SongRepository songRepository;

    @Override
    public void before() {
        songRepository = Objects.create(SongRepository.class);
        song = SongTest.dummy();
    }

    @Test
    public void createSucceeds() {
        when(songRepository.insert(song)).thenReturn(Try.successful(song));
        Try<Song> result = new CreateSong().createSong(song);
        assertTrue(result.isSuccess());
        assertTrue(result.getUnchecked().equals(song));
        verify(songRepository, new Times(1)).insert(song);
    }

}