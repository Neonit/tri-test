package services.resources;

import com.jasongoodwin.monads.Try;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import libs.providers.object.Objects;

public class RootResource implements HttpHandler {

    private final ArtistsResource artistsResource = Objects.create(ArtistsResource.class);
    private final SongsResource songsResource = Objects.create(SongsResource.class);

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
        exchange.getResponseSender().send(createResponse(exchange));
    }

    private String createResponse(HttpServerExchange exchange) {
        return Try.ofFailable(() -> exchange)
                .flatMap(e -> route(e))
                .recover(e -> errorResponse(exchange, e));
    }

    private Try<String> route(HttpServerExchange exchange) {
        if (exchange.getRelativePath().contains("/artists")) {
            return artistsResource.createResponse(exchange);
        } else if (exchange.getRelativePath().contains("/songs")) {
            return songsResource.createResponse(exchange);
        }
        return Try.ofFailable(() -> "Oops, something went wrong");
    }

    private String errorResponse(HttpServerExchange exchange, Throwable e) {
        //TODO implement real responses (with status codes etc)
        exchange.setStatusCode(500);
        return String.format("{ \"error\": \"%s\" }", e.getMessage());
    }
}
