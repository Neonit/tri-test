package libs.exceptions;

public class DuplicateException extends RuntimeException{

    private DuplicateException(String message) {
        super(message);
    }

    public static DuplicateException of() {
        return new DuplicateException("Entity already exists");
    }
}
