package data.gateways;

import com.jasongoodwin.monads.Try;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.util.JSON;
import data.setup.MongoDatabase;
import libs.Json;
import org.apache.commons.io.IOUtils;
import org.bson.Document;

import java.util.List;

public class SongGateway {

    private final MongoCollection<Document> collection = MongoDatabase.getCollection("songs");

    public <T> Try<List<T>> findByGenre(Class<T> type, String genre) {
        return Try.ofFailable(() -> genre)
                .map(g -> JSON.serialize(collection.find(Filters.eq("Genre", genre))))
                .flatMap(j -> Json.toList(j, type));
    }

    public <T> Try<T> insert(T object) {
        return Try.ofFailable(() -> object)
                .flatMap(o -> Json.toJson(o))
                .map(j -> {
                    collection.insertOne(Document.parse(j));
                    return object;
                });
    }

    public Try<Boolean> alreadyExists(String name) {
        return Try.ofFailable(() -> name)
                .map(n -> collection.countDocuments(new Document("Name", n)))
                .map(i -> i != 0);
    }

    public <T> Try<List<T>> getInitial(Class<T> type) {
        return getContentFromInitialFile()
                .flatMap(s -> convertToList(type, s));
    }

    public <T> Try<T> update(T object, String id) {
        return Try.ofFailable(() -> object)
                .flatMap(o -> Json.toJson(o))
                .map(j -> collection.replaceOne(Filters.eq("Id", id), Document.parse(j)))
                .map(r -> object);
    }
    
    public <T> Try<T> delete(T object, String id) {
        return Try.ofFailable(() -> object)
                .flatMap(o -> Json.toJson(o))
                .map(j -> collection.deleteOne(Filters.eq("Id", id)))
                .map(r -> object);
    }

    private Try<String> getContentFromInitialFile() {
        return Try.ofFailable(() -> getClass().getClassLoader().getResource("songs.json"))
                .map(r -> IOUtils.toByteArray(r))
                .map(b -> new String(b));
    }

    private <T> Try<List<T>> convertToList(Class<T> type, String json) {
        return Try.ofFailable(() -> json)
                .flatMap(j -> Json.toList(j, type));
    }
}
