package libs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jasongoodwin.monads.Try;

import java.util.List;

public class Json {

    public static <T> Try<T> toObject(String json, Class<T> type) {
        return Try.ofFailable(() -> json)
                .map(j -> new ObjectMapper().readValue(json, type));
    }

    public static Try<String> toJson(Object object) {
        return Try.ofFailable(() -> object)
                .map(o -> new ObjectMapper().writeValueAsString(o));
    }

    public static <T> Try<List<T>> toList(String json, Class<T> type) {
        return Try.ofFailable(() -> json)
                .map(j -> new ObjectMapper().readValue(json, new ObjectMapper().getTypeFactory().constructCollectionType(List.class, type)));
    }
}
