package domain.repositories;

import com.jasongoodwin.monads.Try;
import data.gateways.SongGateway;
import domain.objects.Song;
import domain.objects.SongTest;
import libs.providers.object.Objects;
import libs.test.TestHelper;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

class SongRepositoryTest extends TestHelper {
    private SongGateway gateway;
    private Song song;
    private List<Song> songs;

    @Override
    public void before() {
        gateway = Objects.create(SongGateway.class);

        song = SongTest.dummy();
        songs = Arrays.asList(song);
    }

    @Test
    public void insertSucceeds() {
        when(gateway.insert(song)).thenReturn(Try.ofFailable(() -> song));
        when(gateway.alreadyExists(song.Name)).thenReturn(Try.ofFailable(() -> false));
        Try<Song> result = new SongRepository().insert(song);
        assertTrue(result.isSuccess());
    }

    @Test
    public void insertDuplicateFails() {
        when(gateway.insert(song)).thenReturn(Try.ofFailable(() -> song));
        when(gateway.alreadyExists(song.Name)).thenReturn(Try.ofFailable(() -> true));
        Try<Song> result = new SongRepository().insert(song);
        assertFalse(result.isSuccess());
    }

    @Test
    public void insertInitialSucceeds() {
        when(gateway.insert(song)).thenReturn(Try.ofFailable(() -> song));
        Try<Song> result = new SongRepository().insertInitial(song);
        assertTrue(result.isSuccess());
    }

    @Test
    public void getInitialSucceeds() {
        when(gateway.getInitial(Song.class)).thenReturn(Try.ofFailable(() -> songs));
        Try<List<Song>> result = new SongRepository().getInitial();
        assertTrue(result.isSuccess());
        assertThat(result.getUnchecked(), is(songs));
    }

    @Test
    public void findByGenreSucceeds() {
        when(gateway.findByGenre(Song.class, song.Genre)).thenReturn(Try.ofFailable(() -> songs));
        Try<List<Song>> result = new SongRepository().findByGenre(song.Genre);
        assertTrue(result.isSuccess());
        assertThat(result.getUnchecked().get(0), is(song));
    }


    @Test
    public void updateSucceeds() {
        when(gateway.update(song, song.Id)).thenReturn(Try.ofFailable(() -> song));
        Try<Song> result = new SongRepository().update(song);
        assertTrue(result.isSuccess());
        assertThat(result.getUnchecked(), is(song));
    }

    @Test
    public void deleteSucceeds() {
        when(gateway.delete(song, song.Id)).thenReturn(Try.ofFailable(() -> song));
        Try<Song> result = new SongRepository().delete(song);
        assertTrue(result.isSuccess());
        assertThat(result.getUnchecked(), is(song));
    }
}