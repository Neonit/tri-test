package data.gateways;

import com.jasongoodwin.monads.Try;
import domain.objects.Song;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;

class SongGatewayTest {
    private SongGateway songGateway;

    @BeforeEach
    private void setup() {
        songGateway = new SongGateway();
    }

    @Test
    public void initialConvertsToListOfSongs() throws Throwable {
        Try<List<Song>> result = songGateway.getInitial(Song.class);
        assertThat(result.get(), instanceOf(List.class));
        assertThat(result.get().size(), not(0));
        assertThat(result.get().get(0).Id, notNullValue());
        assertThat(result.get().get(0).Name, notNullValue());
        assertThat(result.get().get(0).Year, notNullValue());
        assertThat(result.get().get(0).Artist, notNullValue());
        assertThat(result.get().get(0).Shortname, notNullValue());
        assertThat(result.get().get(0).Bpm, notNullValue());
        assertThat(result.get().get(0).Duration, notNullValue());
        assertThat(result.get().get(0).Genre, notNullValue());
        assertThat(result.get().get(0).SpotifyId, notNullValue());
        assertThat(result.get().get(0).Album, notNullValue());
    }
}