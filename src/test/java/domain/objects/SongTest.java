package domain.objects;

import com.jasongoodwin.monads.Try;
import libs.Json;
import libs.test.TestHelper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SongTest extends TestHelper {

    private Song song = dummy();

    public static Song dummy() {
        return Song.of("1", "Name", "1234", "Artist", "Shortname", "123", "90", "Metal", "12345", "Album");
    }

    @Test
    public void jacksonTest() {
        Try<String> input = Json.toJson(song);
        assertTrue(input.isSuccess());
        Try<Song> output = Json.toObject(input.getUnchecked(), Song.class);
        assertTrue(output.isSuccess());
        assertEquals(output.getUnchecked().Id, song.Id);
        assertEquals(output.getUnchecked().Name, song.Name);
        assertEquals(output.getUnchecked().Year, song.Year);
        assertEquals(output.getUnchecked().Artist, song.Artist);
        assertEquals(output.getUnchecked().Shortname, song.Shortname);
        assertEquals(output.getUnchecked().Bpm, song.Bpm);
        assertEquals(output.getUnchecked().Duration, song.Duration);
        assertEquals(output.getUnchecked().Genre, song.Genre);
        assertEquals(output.getUnchecked().SpotifyId, song.SpotifyId);
        assertEquals(output.getUnchecked().Album, song.Album);
    }

}