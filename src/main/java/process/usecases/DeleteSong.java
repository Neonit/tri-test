package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Song;
import domain.repositories.SongRepository;
import libs.providers.object.Objects;

public class DeleteSong {
    private SongRepository songRepository = Objects.create(SongRepository.class);

    public Try<Song> deleteSong(Song song) {
        return songRepository.delete(song);
    }
}
