package process.usecases;

import com.jasongoodwin.monads.Try;
import domain.objects.Artist;
import domain.repositories.ArtistRepository;
import libs.providers.object.Objects;

public class FindArtist {
    private ArtistRepository artistRepository = Objects.create(ArtistRepository.class);

    public Try<Artist> findArtist(String name) {
        return artistRepository.findByName(name);
    }
}
