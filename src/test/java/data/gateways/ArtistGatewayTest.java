package data.gateways;

import com.jasongoodwin.monads.Try;
import domain.objects.Artist;
import libs.test.TestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;

class ArtistGatewayTest extends TestHelper {

    private ArtistGateway artistGateway;

    @BeforeEach
    private void setup() {
        artistGateway = new ArtistGateway();
    }

    @Test
    public void initialConvertsToListOfArtists() throws Throwable {
        Try<List<Artist>> result = artistGateway.getInitial(Artist.class);
        assertThat(result.get(), instanceOf(List.class));
        assertThat(result.get().size(), not(0));
        assertThat(result.get().get(0).Id, notNullValue());
        assertThat(result.get().get(0).Name, notNullValue());
    }
}