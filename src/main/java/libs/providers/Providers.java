package libs.providers;

import com.jasongoodwin.monads.Try;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import static java.util.Arrays.asList;

public class Providers<T> {

    private List<T> providers = new ArrayList<>();

    public void add(T... providers) {
        this.providers.addAll(asList(providers));
    }

    public void clear() {
        providers.clear();
    }

    public <U> Try<U> map(Function<T, U> function) {

        Objects.requireNonNull(function);

        for (T p : providers) {
            Try<U> result = Try.ofFailable(() -> function.apply(p));
            if(!result.isSuccess() || result.getUnchecked() != null)
                return result;
        }
        return Try.failure(new RuntimeException("No provider gives a valid result"));
    }

}
